package datastructure.queue;

import java.util.Collections;
import java.util.PriorityQueue;

public class pQueue {
    public static void main(String[] args) {
//        PriorityQueue<Integer> iPq = new PriorityQueue<>(); // natural order
        PriorityQueue<Integer> iPq = new PriorityQueue<>(Collections.reverseOrder() );
//        PriorityQueue<Integer> iPq = new PriorityQueue<>((a,b)-> b.compareTo(a));
        iPq.add(3);
        iPq.add(2);
        iPq.add(4);
        iPq.add(5);

        while (iPq.iterator().hasNext()){
            System.out.println(iPq.poll());
        }
    }
}
