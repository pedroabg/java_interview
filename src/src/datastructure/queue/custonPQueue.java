package datastructure.queue;

import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.PriorityQueue;

class PersonComparator implements Comparator<Person> {

    public int compare(Person o1, Person o2) {
        return Integer.compare(o2.age,o1.age); //descending
//        return Integer.compare(o1.age,o2.age);
//        return Float.compare(o1.heigh,o2.heigh);
    }
}

class Person  {
    int age;
    float heigh;

    public Person(int age, float heigh) {
        this.age = age;
        this.heigh = heigh;
    }


    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", heigh=" + heigh +
                '}';
    }
}

public class custonPQueue {
    public static void main(String[] args) {
//        PriorityQueue<Integer> iPq = new PriorityQueue<>(); // natural order
        PriorityQueue<Person> personPq = new PriorityQueue<>(new PersonComparator());
//        PriorityQueue<Integer> iPq = new PriorityQueue<>((a,b)-> b.compareTo(a));
        personPq.add(new Person(3,0.5f));
        personPq.add(new Person(13,1.5f));
        personPq.add(new Person(18,1.4f));
        personPq.add(new Person(15,1.9f));
        personPq.add(new Person(13,1.9f));

        while (personPq.iterator().hasNext()){
            System.out.println(personPq.poll());
        }
    }
}
