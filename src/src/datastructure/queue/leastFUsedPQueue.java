package datastructure.queue;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

class Lfu1Comparator implements Comparator<Lfu1> {

    public int compare(Lfu1 o1, Lfu1 o2) {
        return Integer.compare(o1.freq,o2.freq);
    }
}

class Lfu1  {
    int freq;
    int key;

}

public class leastFUsedPQueue {
    public static void main(String[] args) {

        PriorityQueue<Lfu1> lfu = new PriorityQueue<>(new Lfu1Comparator());
        Map<String, Integer> freq = new HashMap<>();
        Map<String, Integer> values = new HashMap<String, Integer>(){
            public Integer put(String k, Integer v){
                freq.compute(k, (c,vv)-> (vv == null) ? 1 : vv + 1 );
                return super.put(k,v);
            }
        };
        int maxSize = 3;

        values.put("Pedro",2);
        values.put("Gomes",2);
        values.put("Pedro",2);
        values.put("Alberto",3);
        values.put("Pedro",1);


        for(Map.Entry m : values.entrySet()){
            System.out.println(m.getKey() + " value " + m.getValue() + " freq " + freq.get(m.getKey()));
        }




    }
}
