package datastructure;

import java.util.HashMap;
import java.util.Iterator;

public class HashMapDemo {

    public static void main(String[] args){
        System.out.println("Demonstrate");
        HashMap<Character, Integer> freq = new HashMap<>();
        String sentence = "Today is the best day of my life!";

        for (Character c: sentence.toCharArray()) {
//            if(!freq.containsKey(c)){
//                freq.put(c,1);
//            }else{
//                freq.put(c, freq.get(c)+1);
//            }

            freq.compute(c, (k,v)-> (v == null) ? 1 : v +1);
        }

        Iterator<Character> it = freq.keySet().iterator();
        while (it.hasNext()){
            Character c = it.next();
            System.out.println(c + " : " + freq.get(c));
        }

    }
}
