package datastructure;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LeastRecentUsedLinked {

    public static void main(String[] args){
        System.out.println("Demonstrate");
        int MAX = 3;
        HashMap<Character, Integer> freq = new LinkedHashMap<Character, Integer>(){
            protected boolean removeEldestEntry(Map.Entry<Character, Integer> eldest)
            {
                return size() > MAX;
            }
        };
        String sentence = "Today is the best day of my life!";

        for (Character c: sentence.toCharArray()) {
            freq.compute(c, (k,v)-> (v == null) ? 1 : v +1);
        }

        for(Map.Entry m : freq.entrySet()){
            System.out.println(m.getKey() + " " + m.getValue() );
        }



    }
}
