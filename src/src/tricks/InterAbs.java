package tricks;

interface BaseI {void method(); void method2(); void method3();}

class BaseC{
    public void method(){
        System.out.println("Inside baseC::method");
    }
    public void method2(){}

    public BaseC method4(){
        Character c = new Character('c');
        return this ;
    }
}

public class InterAbs extends BaseC implements BaseI {
    public void  method3(){}

    public InterAbs method4(){
        Integer i = new Integer(1);
        return this;
    }

    public static void main(String[] args){
        (new InterAbs()).method();
    }
}

/*
* When implementing an interface and extends a class, it is enough if the SUPER class implements the interface methods.
*
* Covariant https://www.tutorialspoint.com/Covariant-return-types-in-Java#:~:text=Covariant%20return%20type%20refers%20to%20return%20type%20of%20an%20overriding%20method.&text=From%20Java%205%20onwards%2C%20we,Following%20example%20showcases%20the%20same.
* */