package randomExercises;
//https://leetcode.com/problems/lfu-cache/submissions/
//        https://leetcode.com/problems/lfu-cache/discuss/1784250/Java-O(1)-Solution-and-How-to-Approach-This-Question


import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

class LFU {
    Map<Integer,Integer> values;
    Map<Integer,Integer> freq;
    Map<Integer, Set<Integer>> minFreqs;
    int capacity;
    int minCounter;

    public LFU(int capacity) {
        this.capacity = capacity;
        this.values = new HashMap<>();
        this.freq = new HashMap<>();
        this.minFreqs = new HashMap<>();
    }

    public int get(int key) {
        Integer value = values.get(key);
        if (value == null) {
            return -1;
        }
        touch(key);
        return value;
    }

    public void put(int key, int value) {
        if (capacity == 0) {
            return;
        }
        if (values.size() == capacity && !values.containsKey(key)) {
            removeMin();
        }
        values.put(key, value);
        touch(key);
    }

    private void touch(int key) {
        int keyCount = freq.compute(key, (k, v) -> (v == null) ? 1 : (v + 1));
//        minFreqs.computeIfAbsent(keyCount, k -> new LinkedHashSet<>()).add(key);
        if(!minFreqs.containsKey(keyCount)){
            Set newFreqSet = new LinkedHashSet<>();
            newFreqSet.add(key);
            minFreqs.put(keyCount, newFreqSet);
        }else {
            Set newFreqSet = minFreqs.get(keyCount);
            newFreqSet.add(key);
            minFreqs.put(keyCount, newFreqSet);
        }


        // Check if the touched number was already there, so it might be also in the last freq
        int prevCount = keyCount - 1;
        if (minFreqs.containsKey(prevCount)) { // we don t have freq 0
            Set<Integer> prevKeySet = minFreqs.get(prevCount);
            prevKeySet.remove(key); // remove key because it went to the next freq
            if (prevKeySet.isEmpty()) {
                minFreqs.remove(prevCount);
                if (prevCount == minCounter) {
                    minCounter = keyCount;
                }
            }
        } else {
            minCounter = keyCount; // New key (i.e. keyCount = 1)
        }
    }

    private void removeMin() {
        Set<Integer> keySet = minFreqs.get(minCounter);
        int keyToEvict = keySet.iterator().next(); // it will get the first of the list = The last used on that line        data.remove(keyToEvict);
        values.remove(keyToEvict);
        keySet.remove(keyToEvict);
        freq.remove(keyToEvict);
        if (keySet.isEmpty()) {
            minFreqs.remove(minCounter);
        }
        // No need to change `minCounter` because `touch()` will set it to 1 later
    }

}


public class LFUleetcode {



     public static void main(String[] args) {
//["LFUCache","put","put","get","put","get","get","put","get","get","get"]
//[[2],[1,1],[2,2],[1],[3,3],[2],[3],[4,4],[1],[3],[4]]

         LFU cache = new LFU(2);
         cache.put(1,1);
         cache.put(2,2);
         System.out.println(cache.get(1));
         cache.put(3,3);
         System.out.println(cache.get(2));
         System.out.println(cache.get(3));

    }
}
