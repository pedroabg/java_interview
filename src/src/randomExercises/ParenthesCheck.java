package randomExercises;

import java.util.ArrayList;
import java.util.List;

public class ParenthesCheck {
    static boolean stackCheck(String target){
        List<Character> stack = new ArrayList();
        if(target.isEmpty()) return false;
        for (char a: target.toCharArray()) {
            if(a == '(')
                stack.add(0,a);
            else {
                if(stack.size() == 0) return false;
                char lastCh = stack.remove(0);
                if (lastCh == ')') return false;
            }
        }
        return stack.size() == 0;
    }

    public static void main(String[] args)
    {
        System.out.println(stackCheck("()"));
        System.out.println(stackCheck("(()"));
        System.out.println(stackCheck(")"));
        System.out.println(stackCheck("(())"));
        System.out.println(stackCheck("(()())"));
    }
}
