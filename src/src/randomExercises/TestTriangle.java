package randomExercises;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


class Triangle {
    int[][] coordinates = new int[3][2];

    public Triangle(int[][] coordinates){
        this.coordinates = coordinates;
    }

    public int[] getVerticePosition(int position){
        return coordinates[position];
    }

    public int[][] getCoordinates(){
        return coordinates;
    }

    public boolean equals(Object o) {
        if (!(o instanceof Triangle)) return false;
        Set<int[]> cordinatesSet = new HashSet<>();
        cordinatesSet.add(this.coordinates[0]);
        cordinatesSet.add(this.coordinates[1]);
        cordinatesSet.add(this.coordinates[2]);
        return cordinatesSet.containsAll(Arrays.asList(((Triangle) o).getCoordinates()));
    }
}

public class TestTriangle {
    public static void main(String[] args)
    {
        int[] xy1A = {0,1};
        int[] xy2A = {1,1};
        int[] xy3A = {0,0};
        int[][] coordA = {xy1A, xy2A, xy3A};
        Triangle A = new Triangle(coordA);
        int[][] coordB = {xy2A, xy1A, xy3A};
        Triangle B = new Triangle(coordB);

        int[] xy3C = {1,0};

        int[][] coordC = {xy2A, xy3C, xy3A};
        Triangle C = new Triangle(coordC);
        System.out.println(A.equals(B));
        System.out.println(A.equals(C));
        System.out.println(B.equals(A));

        System.out.println(B.getVerticePosition(1)[0]);
    }
}
