package Exceptions;
// extension,  exception specific extends order

// will print BCD


public class gofirst {
    public static void main(String[] args){
        try {
            badMethod();
            System.out.println("A");
        }catch ( Exception e ){
            System.out.println("B");
        } finally {
            System.out.println("C");
        }
        System.out.println("D");
    }

    public static void badMethod(){
        throw new RuntimeException();
    }
}
