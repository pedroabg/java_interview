package basic;

public class CaloiBicycle extends AbsVehicle implements Bicycle {
    @Override
    public void changeCadence(int newValue) {

    }

    @Override
    public void changeGear(int newValue) {

    }

    @Override
    public void speedUp(int increment) {

    }

    @Override
    public void applyBrakes(int decrement) {

    }

    @Override
    public void introduction() {
       // super.introduction();
        System.out.println("Now, inside the class");
    }

    public static void main(String[] args){
        CaloiBicycle my = new CaloiBicycle();
        AbsVehicle parent = new CaloiBicycle();
        my.applyBrakes(10);
        my.introduction();
        parent.introduction();

    }
}
