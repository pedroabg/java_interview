package basic;

public class ThreadTest extends Thread {
    public void run()
    {
        System.out.println("Current thread name: "
                + Thread.currentThread().getName());
        System.out.println("run() method called");

        for (int i = 0; i < 100000; i++) {
            System.out.println("Current thread name: "
                    + Thread.currentThread().getName()+" interaction: "+i);
        }
    }
}

class TestThread  {
    public static void main(String[] args)
    {
        ThreadTest t = new ThreadTest();
        ThreadTest t2 = new ThreadTest();
        //t.start();
        t.start();
        //t.run();
        t2.start();
    }
}
