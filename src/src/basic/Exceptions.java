package basic;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Exceptions {


    private static final int SIZE = 10;
    List<Integer> list;

    private void ListOfNumbers () {
        list = new ArrayList<Integer>(SIZE);
        for (int i = 0; i < SIZE; i++) {
            list.add(new Integer(i));
        }
    }

    public void writeList() {
        PrintWriter out = null;

        try {
            System.out.println("Entering try statement");
            out = new PrintWriter(new FileWriter("OutFile.txt"));
            //for (int i = 0; i <= SIZE; i++)
            for (int i = 0; i < SIZE; i++)
                out.println("Value at: " + i + " = " + list.get(i));

        } catch (IndexOutOfBoundsException e) {
            System.err.println("Caught IndexOutOfBoundsException: "
                    + e.getMessage());

        } catch (IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        } finally {
            System.out.println("Finally Block");
            if (out != null) {
                System.out.println("Closing PrintWriter");
                out.close();
            }
            else {
                System.out.println("PrintWriter not open");
            }
        }
    }

    public void writeList3(PrintWriter out) throws IOException , IndexOutOfBoundsException {

        out = new PrintWriter(new FileWriter("OutFile.txt"));
        //for (int i = 0; i <= SIZE; i++)
        for (int i = 0; i <= SIZE; i++)
            out.println("Value at: " + i + " = " + list.get(i));
    }

    public void writeList2() {
        PrintWriter out = null;

        try {
            writeList3(out);

        } catch (IndexOutOfBoundsException e) {
            System.out.println("Entering IndexOutOfBoundsException");
            System.err.println("Caught IndexOutOfBoundsException: "
                    + e.getMessage());

        } catch (IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        } finally {
            System.out.println("Finally Block");
            if (out != null) {
                System.out.println("Closing PrintWriter");
                out.close();
            }
            else {
                System.out.println("PrintWriter not open");
            }
        }
    }

    public static void main(String[] args){
        System.out.println("Let s test exceptions");

        System.out.println("If you try to compile the ListOfNumbers class, the compiler prints an error message about the exception " +
                "thrown by the FileWriter constructor. However, it does not display an error message about the exception thrown by get." +
                " The reason is that the exception thrown by the constructor, IOException, is a checked exception, and the one thrown by the get method, " +
                "IndexOutOfBoundsException, is an unchecked exception.");

        System.out.println("The finally block always executes when the try block exits. " +
                "This ensures that the finally block is executed even if an unexpected exception occurs. " +
                "But finally is useful for more than just exception handling — it allows the programmer to avoid having " +
                "cleanup code accidentally bypassed by a return, continue, or break. " +
                "Putting cleanup code in a finally block is always a good practice, even when no exceptions are anticipated.");

        Exceptions ex = new Exceptions();
        ex.ListOfNumbers();
        ex.writeList();

    Exceptions ex2 = new Exceptions();
        ex2.ListOfNumbers();
        ex2.writeList2();


    }
}
