// vargargs order methods
package basic;

import java.util.List;

public class varArgs {


    public void execute(int[]... values){
        for (int[] val : values) {
            System.out.print(val[0]);
        }
    }
    public void exStr(String... values){
        System.out.print(values[0] + " 1");
    }
    public void exStr(String value){
        System.out.print(value + " 2");
    }

    public static void main(String[] args){
        int[] x = {1,2,3};
        int[] y = {4,5,6};

        varArgs stringVarArgsTest = new varArgs();
        stringVarArgsTest.execute(x,y);
        System.out.println();
        stringVarArgsTest.exStr("Pedro");

    }
}
