package basic;

class Thread1 extends Thread {
    Triangle t1, t2;
    int sleep;
    public Thread1(Triangle t1, Triangle t2, int sleep){
        this.t1 = t1;
        this.t2 = t2;
    }
    public void run()
    {
        synchronized (t1){
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+" using : " + t1.name);
            synchronized (t2){
                System.out.println(Thread.currentThread().getName()+" using : " + t2.name);
            }
        }
    }
}

class Triangle {
    String name;
    public Triangle(String name){
        this.name = name;
    }
}

public class TestDeadLock  {
    public static void main(String[] args)
    {
        Triangle triangleA =  new Triangle("triangleA");
        Triangle triangleB =  new Triangle("triangleB");
        Thread1 t = new Thread1(triangleA, triangleB, 100);
        Thread1 t2 = new Thread1(triangleB, triangleA, 0);
        t.start();
        t2.start();
    }
}
