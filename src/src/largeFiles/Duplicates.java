package largeFiles;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashSet;
import java.util.Scanner;

public class Duplicates {
    public static void main(String[] args) {


        FileInputStream inputStream = null;
        Scanner sc = null;
        try {
            try {

                HashSet<Integer> noDups = new HashSet<>();

                inputStream = new FileInputStream("C:\\Users\\55919\\IdeaProjects\\java_interview\\src\\src\\largeFiles\\numbersDup.txt");
                sc = new Scanner(inputStream, "UTF-8");
                while (sc.hasNextLine()) {
//                    String line = sc.nextLine();
                    int n = sc.nextInt();
                    System.out.println(n);

                    noDups.add(n);
                }


                FileOutputStream outputStream = new FileOutputStream("C:\\Users\\55919\\IdeaProjects\\java_interview\\src\\src\\largeFiles\\numbersNoDup.txt");
                StringBuilder sb = new StringBuilder();

                for (int num: noDups) {
                    sb.append(num+"\n");
                }
                String str = sb.toString();
                byte[] strToBytes = str.getBytes();
                outputStream.write(strToBytes);

                outputStream.close();


                // note that Scanner suppresses exceptions
                if (sc.ioException() != null) {
                    throw sc.ioException();
                }
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (sc != null) {
                    sc.close();
                }
            }
        } catch (Exception e){

        }




    }
}
