package search;

import java.util.Arrays;

public class Binary {

    public int runBinarySearchIteratively(
            int[] sortedArray, int key, int low, int high) {
        int index = Integer.MAX_VALUE;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (sortedArray[mid] < key) {
                low = mid + 1;
            } else if (sortedArray[mid] > key) {
                high = mid - 1;
            } else if (sortedArray[mid] == key) {
                index = mid;
                break;
            }
        }
        return index;
    }

    public static void main(String[] args){
        int[] a = {1,2,3,4,5,6,7,8};
        Binary bin = new Binary();

        int r = bin.runBinarySearchIteratively(a,8,0,a.length-1);
        System.out.println(r);

        r = Arrays.binarySearch(a,6);

    }


}
