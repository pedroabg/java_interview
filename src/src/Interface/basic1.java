package Interface;
// Tags : interface, heritage , extends, implements

interface BaseI{ void method(); }

class BaseC{
    public void method(){
        System.out.println("Inside BaseC::method");
    }
}

class BaseD implements  BaseI{
    public void method(){
        System.out.println("Inside BaseC::method");
    }
}

public class basic1 extends BaseC implements BaseI{
    public static void main(String[] args){

        (new basic1()).method();

        BaseI interVar = new BaseD();
        interVar.method();
    }
}
